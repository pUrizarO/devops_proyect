package com.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsProyectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsProyectApplication.class, args);
	}

}
