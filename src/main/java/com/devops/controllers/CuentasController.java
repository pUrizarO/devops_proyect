package com.devops.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CuentasController {
	
	@GetMapping("/index")
	public String mostrarHome(Model model) {
		
		return "index";
	}
}
